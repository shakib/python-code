class Employee:
    def __init__(self,first_name,second_name,sallary):
        self.f1name = first_name
        self.f2name = second_name
        self.payment = sallary

    def full_name(self):
        return '{} {}'.format(self.f1name,self.f2name)

e1 = Employee('Shakib','Hassan','100000')
e2 = Employee('Abudur','Rahman','50000')
print(Employee.full_name(e1))
print(e2.full_name())
print(e1.payment)

