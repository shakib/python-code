class Employee:
    def __init__(self,first_name,second_name,sallary):
        self.f1name = first_name
        self.f2name = second_name
        self.payment = sallary

    def full_name(self):
        return '{} {}'.format(self.f1name,self.f2name)

    def apply_raise(self):
        self.payment = (int(self.payment) * 1.25)
       #return self.payment 
e1 = Employee('Shakib','Hassan','100000')
e2 = Employee('Abudur','Rahman','50000')


print(e1.payment)
#print(Employee.apply_raise(e1))
e1.apply_raise()
print(e1.payment)
